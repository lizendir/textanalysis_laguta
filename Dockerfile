FROM node

WORKDIR /app

COPY /server/package.json /server/yarn.lock ./

RUN yarn

COPY ./server .
COPY .env .

EXPOSE 4000

CMD ["npm", "run", "serve"]