import gql from 'graphql-tag';
import { SongFieldsFragment } from './fragments';

export const ADD_SONG = gql`
  mutation addSong($song: SongInput!) {
    addSong(song: $song) {
      ...SongFieldsFragment
    }
  }
  ${SongFieldsFragment}
`;
