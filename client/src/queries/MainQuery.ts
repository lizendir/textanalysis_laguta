import gql from 'graphql-tag';

import { SongFieldsFragment } from './fragments';

export const GET_SONGS = gql`
  query getSongs {
    getSongs {
      ...SongFieldsFragment
    }
  }
  ${SongFieldsFragment}
`;

export const GET_USER_SONGS = gql`
  query getUserSongs($userId: Int!) {
    getUserSongs(userId: $userId) {
      ...SongFieldsFragment
    }
  }
  ${SongFieldsFragment}
`;

export const GET_WORDS = gql`
  query getWords {
    getWords {
      id
      songs {
        ...SongFieldsFragment
      }
      name
      count
    }
  }
  ${SongFieldsFragment}
`;

export const GET_WORDS_OF_SONG = gql`
  query getWordsOfSong($songId: Int!) {
    getWordsOfSong(songId: $songId) {
      id
      songs {
        ...SongFieldsFragment
      }
      name
      count
    }
  }
  ${SongFieldsFragment}
`;
