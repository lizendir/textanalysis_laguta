import gql from 'graphql-tag';

export const SongFieldsFragment = gql`
  fragment SongFieldsFragment on Song {
    id
    name
    text
    wordCount
    userId
  }
`;

export const WordsFieldsFragment = gql`
  fragment WordsFieldsFragment on Words {
    id
    name
    count
    songs {
      ...SongFieldsFragment
    }
  }
  ${SongFieldsFragment}
`;
