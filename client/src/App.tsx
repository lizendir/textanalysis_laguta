import React from 'react';
import { CssBaseline } from '@material-ui/core';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import NavBar from './components/NavBar';
import { Footer } from './components/Footer';
import { useStyles } from './styles/contentStyles';

import AuthRoute from './utils/AuthRoute';
import Login from './pages/Login';
import Register from './pages/Register';
import ContextProvider from './context';
import Main from './pages/Songs';
import AddSong from './pages/AddSong';
import Words from './pages/Words';
import SongDetails from './pages/SongDetails';
import YourSongs from './pages/YourSongs';
import { GET_WORDS } from './queries/MainQuery';

export default function MainPage() {
  const { footer, main, root, content } = useStyles();

  return (
    <ContextProvider>
      <div className={root}>
        <Router>
          <CssBaseline />
          <Route component={NavBar} />
          <div className={content}>
            <main className={main}>
              <Route exact path="/your" component={YourSongs} />
              <Route exact path="/details" component={SongDetails} />
              <Route
                exact
                path="/wordsrate"
                render={(props) => {
                  return <Words props={props} QUERY={GET_WORDS} />;
                }}
              />
              <Route exact path="/add" component={AddSong} />
              <Route exact path="/" component={Main} />
              <AuthRoute exact path="/login" component={Login} />
              <AuthRoute exact path="/register" component={Register} />
            </main>
            <footer className={footer}>
              <Footer />
            </footer>
          </div>
        </Router>
      </div>
    </ContextProvider>
  );
}
