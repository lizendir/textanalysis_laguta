import React, { FC } from 'react';
import { useStyles } from '../styles/contentStyles';
import { CircularProgress } from '@material-ui/core';

const Progress: FC<{}> = () => {
  const { progress } = useStyles();
  return (
    <div className={progress}>
      <CircularProgress />
    </div>
  );
};

export default Progress;
