import React, { useContext, FC } from 'react';
import { Button } from '@material-ui/core';
import IconAdd from '@material-ui/icons/Add';
import { useStyles } from '../styles/contentStyles';
import { AuthContext } from '../context/auth';

export const AddButton: FC<{ props: any }> = ({ props }) => {
  const classes = useStyles();
  const authContext = useContext(AuthContext);

  function onClickAdd() {
    if (authContext?.user) {
      props.history.push('/add');
    } else {
      props.history.push('/register');
    }
  }

  return (
    <div className={classes.addButtonContainer}>
      <p>Add song</p>
      <Button
        className={classes.addButton}
        size="small"
        color="primary"
        onClick={onClickAdd}
      >
        <IconAdd />
      </Button>
    </div>
  );
};
