import React, { useContext } from 'react';
import {
  AppBar,
  Toolbar,
  Typography,
  ButtonGroup,
  Button,
  Drawer,
  Divider,
  List,
  ListItem,
  ListItemText,
} from '@material-ui/core';
import { useStyles } from '../styles/contentStyles';
import { AuthContext } from '../context/auth';

export default function (props: any) {
  const classes = useStyles();
  const authContext = useContext(AuthContext);

  const AuthBarItem = authContext?.user ? (
    <>
      <Typography className={classes.logRegTabs} color="inherit">
        {authContext?.user ? authContext.user.username : 'Unknown'}
      </Typography>

      <ButtonGroup
        className={classes.logRegTabs}
        color="inherit"
        aria-label="outlined primary button group"
      >
        <Button title="Logout" onClick={authContext.logout}>
          Logout
        </Button>
      </ButtonGroup>
    </>
  ) : (
    <ButtonGroup
      className={classes.logRegTabs}
      color="inherit"
      aria-label="outlined primary button group"
    >
      <Button title="Login" href="/login">
        Login
      </Button>
      <Button title="Register" href="/register">
        Register
      </Button>
    </ButtonGroup>
  );

  function redirectTo(path: string) {
    if (authContext?.user) {
      props.history.push(`/${path}`);
    } else {
      props.history.push('/register');
    }
  }

  const drawer = (
    <>
      <Toolbar />
      <Divider className={classes.divider} />
      <List>
        <ListItem button onClick={(e) => redirectTo('add')}>
          <ListItemText primary="Add Song" />
        </ListItem>
        <ListItem button onClick={(e) => redirectTo('your')}>
          <ListItemText primary="Your Songs" />
        </ListItem>
      </List>
      <Divider className={classes.divider} />
      <List>
        <ListItem button onClick={(e) => props.history.push('/')}>
          <ListItemText primary="Songs Rate" />
        </ListItem>
        <ListItem button onClick={(e) => props.history.push('/wordsrate')}>
          <ListItemText primary="Words Rate" />
        </ListItem>
      </List>
    </>
  );

  return (
    <>
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar className={classes.toolbar}>
          <Typography
            className={classes.navbar}
            variant="h5"
            color="inherit"
            noWrap
          >
            Text Analysis Application
          </Typography>
          {AuthBarItem}
        </Toolbar>
      </AppBar>
      <Drawer
        className={classes.drawer}
        variant="permanent"
        anchor="left"
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        {drawer}
      </Drawer>
    </>
  );
}
