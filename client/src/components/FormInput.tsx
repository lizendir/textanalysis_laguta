import React, { FC } from 'react';
import { FormControl, InputLabel, Input } from '@material-ui/core';

interface Props {
  label: string;
  name: string;
  value: string;
  isError?: boolean;
  multiline?: boolean;
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

export const FormInput: FC<Props> = ({
  isError = false,
  label,
  name,
  multiline = false,
  onChange,
  value,
}) => {
  return (
    <FormControl style={{ marginBottom: 10 }}>
      <InputLabel>{label}</InputLabel>
      <Input
        name={name}
        value={value}
        error={isError}
        onChange={onChange}
        multiline={multiline}
      />
    </FormControl>
  );
};
