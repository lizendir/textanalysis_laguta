import React, { FC } from 'react';
import Alert from '@material-ui/lab/Alert';
import { isString } from 'util';

interface Props {
  errors: any;
}

export const ErrorLabels: FC<Props> = ({ errors }) => {
  return (
    <>
      {Object.values(errors).filter((value) => {
        if (value) return value;
        else return null;
      }).length > 0 && (
        <div>
          <ul>
            {Object.values(errors).map((value, index) => (
              <Alert key={index} severity="error">
                {isString(value) ? value : 'An error occured'}
              </Alert>
            ))}
          </ul>
        </div>
      )}
    </>
  );
};
