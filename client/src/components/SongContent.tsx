import React, { useState } from 'react';
import { Song } from '../models';
import { message } from '../utils/message';
import {
  TableContainer,
  Paper,
  Table,
  TableRow,
  TableCell,
  TableHead,
  TableBody,
  Button,
  TableSortLabel,
} from '@material-ui/core';
import {
  SongColumns,
  setSongSortValues,
  sortSongs,
  Order,
} from '../utils/sort';

interface GetSongContent {
  props: any;
  songs: Song[];
  buttonClass: string;
  tableClass: string;
  setSong: ((song: Song | null) => {}) | undefined;
}

function SongContent({
  buttonClass,
  props,
  setSong,
  songs,
  tableClass,
}: GetSongContent) {
  const [sortOrder, setSortOrder] = useState<Order>(undefined);
  const [sortColumn, setSortColumn] = useState<SongColumns>(undefined);

  if (!setSong) {
    return message('Song no chosen');
  }

  function onClickDetails(song: Song) {
    if (setSong) {
      setSong(song);
    }
    props.history.push('/details');
  }

  function onSort(column: SongColumns) {
    if (column === sortColumn) {
      if (sortOrder === 'asc') {
        setSortOrder('desc');
      } else {
        setSortOrder('asc');
      }
    } else {
      setSortColumn(column);
    }
    setSongSortValues(sortColumn, sortOrder);
  }

  if (songs.length < 1) {
    return message('No songs');
  } else {
    return (
      <TableContainer className={tableClass} component={Paper}>
        <Table size="small" aria-label="a dense table">
          <TableHead>
            <TableRow>
              <TableCell
                sortDirection={sortColumn === 'name' ? sortOrder : undefined}
              >
                <TableSortLabel
                  direction={sortColumn === 'name' ? sortOrder : undefined}
                  onClick={(e) => onSort('name')}
                >
                  Name
                </TableSortLabel>
              </TableCell>
              <TableCell
                sortDirection={
                  sortColumn === 'wordCount' ? sortOrder : undefined
                }
                align="right"
              >
                <TableSortLabel
                  direction={sortColumn === 'wordCount' ? sortOrder : undefined}
                  onClick={(e) => onSort('wordCount')}
                >
                  Count
                </TableSortLabel>
              </TableCell>
              <TableCell></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {songs
              .sort((a, b) => {
                return sortSongs(a, b);
              })
              .map((song) => (
                <TableRow key={song.id}>
                  <TableCell component="th" scope="word">
                    {song.name}
                  </TableCell>
                  <TableCell align="right">{song.wordCount}</TableCell>
                  <TableCell>
                    <Button
                      className={buttonClass}
                      onClick={(e) => onClickDetails(song)}
                    >
                      Details
                    </Button>
                  </TableCell>
                </TableRow>
              ))}
          </TableBody>
        </Table>
      </TableContainer>
    );
  }
}

export default SongContent;
