import { makeStyles } from '@material-ui/core';

const drawerWidth = 120;
const appWidth = 1100;

export const useStyles = makeStyles((theme) => ({
  /* Roots Elements */

  root: {
    display: 'flex',
    maxWidth: appWidth,
    marginRight: 'auto',
    marginLeft: 'auto',
    marginTop: 65,
    background: theme.palette.background.default,
  },
  main: {
    flexGrow: 1,
    padding: theme.spacing(3),
    marginLeft: drawerWidth,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  content: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
  },
  footer: {
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing(6),
    marginLeft: drawerWidth,
  },

  /* Auth Forms */

  form: {
    display: 'flex',
    flexDirection: 'column',
    '& > *': {
      margin: theme.spacing(1),
    },
    alignItems: 'center',
  },
  formTitle: {
    fontSize: 24,
  },
  logRegTabs: {
    margin: theme.spacing(1),
    marginLeft: 'auto',
  },

  /* Buttons */

  btn: {
    padding: 0,
  },

  addButton: {
    margin: 10,
  },

  /* NavBar */

  navbar: {
    margin: 'auto',
    color: theme.palette.background.default,
    marginLeft: 0,
  },

  appBar: {
    display: 'flex',
    alignItems: 'center',
    background: theme.palette.background.default,
    boxShadow: 'none',
  },

  toolbar: {
    background: 'grey',
    width: '100%',
    maxWidth: appWidth,
  },

  /* Songs */

  divider: {
    margin: 5,
    backgroundColor: 'white',
  },
  progress: {
    display: 'flex',
    justifyContent: 'center',
  },
  drawerPaper: {
    width: drawerWidth,
    background: 'black',
    color: 'aqua',
    left: 'auto',
    position: 'inherit',
    marginTop: 20,
  },
  drawer: {
    flexShrink: 0,
    position: 'fixed',
  },
  inputItem: {
    width: '80%',
  },
  addButtonContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    cursor: 'default',
  },

  /* Words */
  tableContainer: {
    maxWidth: 600,
  },
  popover__item: {
    width: 150,
    textAlign: 'center',
  },
  popover__firstItem: {
    width: 150,
    marginLeft: 10,
  },
}));
