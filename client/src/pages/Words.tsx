import React, { useState } from 'react';
import {
  TableContainer,
  Paper,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Popover,
  Button,
  TableSortLabel,
} from '@material-ui/core';
import { useQuery } from 'react-apollo';

import { QueryWords, Song, Words } from '../models';
import { useStyles } from '../styles/contentStyles';
import { message } from '../utils/message';
import Progress from '../components/Progress';
import {
  WordColumns,
  Order,
  sortWords,
  setWordsSortValues,
} from '../utils/sort';
import { DocumentNode } from 'graphql';

interface WordsProps {
  props: any;
  QUERY: DocumentNode;
  variables?: {};
}

export default function WordsPage({ props, QUERY, variables }: WordsProps) {
  const classes = useStyles();

  const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);
  const [popoverContent, setPopoverContent] = useState(<p></p>);
  const [sortOrder, setSortOrder] = useState<Order | undefined>(undefined);
  const [sortColumn, setSortColumn] = useState<WordColumns | undefined>(
    undefined,
  );

  const { data, loading } = useQuery<QueryWords>(QUERY, {
    variables: variables,
    onError(err) {
      message(err.message);
    },
  });

  if (loading) {
    return <Progress />;
  }
  if (!data) {
    return message('No data');
  }

  function onClickPopover(
    event: React.MouseEvent<HTMLButtonElement>,
    songs: Song[] | undefined,
  ) {
    setAnchorEl(event.currentTarget);
    const content = songs ? (
      <>
        <p className={classes.popover__firstItem}>
          <b>This word was in:</b>
        </p>
        {songs.map((song) => {
          return <p className={classes.popover__item}>{song.name}</p>;
        })}
      </>
    ) : (
      <p></p>
    );
    setPopoverContent(content);
  }

  function onClosePopover() {
    setAnchorEl(null);
  }

  function onSort(column: WordColumns) {
    if (column === sortColumn) {
      if (sortOrder === 'asc') {
        setSortOrder('desc');
      } else {
        setSortOrder('asc');
      }
    } else {
      setSortColumn(column);
    }
    setWordsSortValues(sortColumn, sortOrder);
  }

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;

  console.log(data);

  let words: Words[];
  if (data.getWords) {
    words = data.getWords;
  } else {
    words = data.getWordsOfSong;
  }

  return (
    <TableContainer className={classes.tableContainer} component={Paper}>
      <Table size="small" aria-label="a dense table">
        <TableHead>
          <TableRow>
            <TableCell
              sortDirection={sortColumn === 'name' ? sortOrder : undefined}
            >
              <TableSortLabel
                direction={sortColumn === 'name' ? sortOrder : undefined}
                onClick={(e) => onSort('name')}
              >
                Name
              </TableSortLabel>
            </TableCell>
            <TableCell
              sortDirection={sortColumn === 'count' ? sortOrder : undefined}
              align="right"
            >
              <TableSortLabel
                direction={sortColumn === 'count' ? sortOrder : undefined}
                onClick={(e) => onSort('count')}
              >
                Count
              </TableSortLabel>
            </TableCell>
            <TableCell align="right">In songs</TableCell>
          </TableRow>
        </TableHead>
        <Popover
          id={id}
          open={open}
          anchorEl={anchorEl}
          onClose={onClosePopover}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'center',
          }}
        >
          {popoverContent}
        </Popover>
        <TableBody>
          {words
            .sort((a, b) => {
              return sortWords(a, b);
            })
            .map((word) => (
              <TableRow key={word.id}>
                <TableCell component="th" scope="word">
                  {word.name}
                </TableCell>
                <TableCell align="right">{word.count}</TableCell>
                <TableCell align="right">
                  <Button
                    className={classes.btn}
                    aria-describedby={id}
                    color="primary"
                    onClick={(e) => onClickPopover(e, word.songs)}
                  >
                    Watch
                  </Button>
                </TableCell>
              </TableRow>
            ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
