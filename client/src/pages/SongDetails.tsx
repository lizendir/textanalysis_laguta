import React, { useContext } from 'react';
import { TextField, Container } from '@material-ui/core';
import { useStyles } from '../styles/contentStyles';
import { SongContext } from '../context/song';
import { message } from '../utils/message';
import Words from './Words';
import { GET_WORDS_OF_SONG } from '../queries/MainQuery';

export default function SongDetails(props: any) {
  const classes = useStyles();
  const songContext = useContext(SongContext);

  const song = songContext?.song;

  if (!song) {
    return message('Song not chosen');
  }

  return (
    <Container className={classes.form}>
      <TextField
        name="name"
        label="Name of song"
        className={classes.inputItem}
        value={song.name}
        disabled
      />
      <TextField
        className={classes.inputItem}
        label="Lyrics"
        multiline
        name="text"
        rowsMax={30}
        value={song.text}
        variant="outlined"
        disabled
      />
      <Words
        props={props}
        QUERY={GET_WORDS_OF_SONG}
        variables={{ songId: song.id }}
      />
    </Container>
  );
}
