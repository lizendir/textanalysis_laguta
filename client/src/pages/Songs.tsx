import React, { useContext } from 'react';
import { useQuery } from 'react-apollo';

import { QuerySongs } from '../models';
import { AddButton } from '../components/AddButton';
import { GET_SONGS } from '../queries/MainQuery';
import { message } from '../utils/message';
import SongContent from '../components/SongContent';
import Progress from '../components/Progress';
import { SongContext } from '../context/song';
import { useStyles } from '../styles/contentStyles';

export default function Songs(props: any) {
  const { btn, tableContainer } = useStyles();
  const songContext = useContext(SongContext);

  const { data, loading } = useQuery<QuerySongs>(GET_SONGS, {
    onError(err) {
      return message(err.message);
    },
  });

  if (loading) {
    return <Progress />;
  }

  const songs = data?.getSongs || [];

  return (
    <>
      <AddButton props={props} />
      <SongContent
        props={props}
        buttonClass={btn}
        tableClass={tableContainer}
        setSong={songContext?.setSong}
        songs={songs}
      />
    </>
  );
}
