import React, { useContext } from 'react';
import { useQuery } from 'react-apollo';

import { QueryYourSongs } from '../models';
import { useStyles } from '../styles/contentStyles';
import { AuthContext } from '../context/auth';
import { SongContext } from '../context/song';

import { AddButton } from '../components/AddButton';
import { GET_USER_SONGS } from '../queries/MainQuery';
import SongContent from '../components/SongContent';
import { message } from '../utils/message';
import Progress from '../components/Progress';

export default function YourSongs(props: any) {
  const { btn, tableContainer } = useStyles();

  const songContext = useContext(SongContext);
  const authContext = useContext(AuthContext);

  const { data, loading } = useQuery<QueryYourSongs>(GET_USER_SONGS, {
    onError(err) {
      return message(err.message);
    },
    variables: {
      userId: authContext?.user?.id,
    },
  });

  if (loading) {
    return <Progress />;
  }

  const songs = data?.getUserSongs || [];

  return (
    <>
      <AddButton props={props} />
      <SongContent
        props={props}
        buttonClass={btn}
        tableClass={tableContainer}
        setSong={songContext?.setSong}
        songs={songs}
      />
    </>
  );
}
