import React, { useContext, useState } from 'react';
import { TextField, Container, Button } from '@material-ui/core';
import { useStyles } from '../styles/contentStyles';
import { AuthContext } from '../context/auth';
import { useMutation } from 'react-apollo';
import { ADD_SONG } from '../queries/Mutation';
import { GET_SONGS, GET_USER_SONGS } from '../queries/MainQuery';
import Progress from '../components/Progress';
import { SongContext } from '../context/song';

export default function AddSong(props: any) {
  const classes = useStyles();

  const [name, setName] = useState('');
  const [text, setText] = useState('');
  const authContext = useContext(AuthContext);
  const songContext = useContext(SongContext);

  const [addSong, { loading }] = useMutation(ADD_SONG, {
    update(proxy, result) {
      try {
        const data: any = proxy.readQuery({
          query: GET_SONGS,
          variables: null,
        });
        data.getSongs = [result.data.addSong, ...data.getSongs];
        proxy.writeQuery({ query: GET_SONGS, data });
      } catch (err) {
        console.log('catch song query');
        console.log(err);
      }
      try {
        if (authContext?.user?.id) {
          const data: any = proxy.readQuery({
            query: GET_USER_SONGS,
            variables: { userId: authContext.user?.id },
          });
          data.getUserSongs = [result.data.addSong, ...data.getUserSongs];
          proxy.writeQuery({
            query: GET_USER_SONGS,
            data,
            variables: { userId: authContext.user?.id },
          });
        }
      } catch (err) {
        console.log('catch user song query');
        console.log(err);
      }
      songContext?.setSong(null);
      props.history.push('/');
    },
    onError(err) {
      console.log('onError');
      console.log(err);
    },
    variables: {
      song: {
        name,
        text,
        userId: authContext?.user?.id,
      },
    },
  });

  function onClickAdd() {
    if (authContext?.user) {
      addSong();
    } else {
      props.history.push('/register');
    }
  }

  if (loading) {
    return <Progress />;
  }

  return (
    <Container className={classes.form}>
      <TextField
        className={classes.inputItem}
        name="name"
        label="Name of song"
        value={name}
        onChange={(e) => setName(e.target.value)}
      />
      <TextField
        className={classes.inputItem}
        label="Lyrics"
        multiline
        name="text"
        rowsMax={30}
        value={text}
        onChange={(e) => setText(e.target.value)}
        variant="outlined"
      />
      <Button variant="outlined" onClick={onClickAdd}>
        Add
      </Button>
    </Container>
  );
}
