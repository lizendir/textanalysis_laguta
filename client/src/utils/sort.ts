import { Words, Song } from '../models';

export type Order = 'asc' | 'desc' | undefined;
export type WordColumns = 'name' | 'count' | undefined;
export type SongColumns = 'name' | 'wordCount' | undefined;

let sortWordColumns: WordColumns = undefined;
let sortOrder: Order = undefined;
let sortSongColumns: SongColumns = undefined;

export function setWordsSortValues(
  column: WordColumns | undefined,
  order: Order | undefined,
) {
  sortWordColumns = column;
  sortOrder = order;
}

export function sortWords(a: Words, b: Words) {
  if (sortWordColumns) {
    if (sortOrder === 'asc') {
      return a[sortWordColumns] > b[sortWordColumns] ? -1 : 0;
    }
    if (sortOrder === 'desc') {
      return a[sortWordColumns] < b[sortWordColumns] ? -1 : 0;
    }
  }
  return 0;
}

export function setSongSortValues(
  column: SongColumns | undefined,
  order: Order | undefined,
) {
  sortSongColumns = column;
  sortOrder = order;
}

export function sortSongs(a: Song, b: Song) {
  if (sortSongColumns) {
    if (sortOrder === 'asc') {
      return a[sortSongColumns] > b[sortSongColumns] ? -1 : 0;
    }
    if (sortOrder === 'desc') {
      return a[sortSongColumns] < b[sortSongColumns] ? -1 : 0;
    }
  }
  return 0;
}
