import * as React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { AuthContext } from '../context/auth';

interface AuthRouteProps {
  exact: true;
  path: string;
  component: (props: any) => JSX.Element;
}

export default ({ component, ...rest }: AuthRouteProps) => {
  const context = React.useContext(AuthContext);
  const Component = component;

  return (
    <Route
      {...rest}
      render={(props) =>
        context?.user ? <Redirect to="/" /> : <Component {...props} />
      }
    />
  );
};
