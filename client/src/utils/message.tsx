import React from 'react';
import { Typography } from '@material-ui/core';

export const message = (text: string) => {
  return (
    <Typography variant="h5" style={{ marginTop: 20 }} align="center">
      {text}
    </Typography>
  );
};
