export interface Song {
  id: number;
  name: string;
  text: string;
  wordCount: number;
  userId: number;
  createdAt: Date;
  updatedAt: Date;
}

export interface SongInput {
  name: string;
  text: string;
  userId: number;
}

export interface Words {
  id: number;
  name: string;
  songs?: Song[];
  count: number;
  createdAt: Date;
  updatedAt: Date;
}

export interface QuerySongs {
  getSongs: Song[];
}

export interface QueryYourSongs {
  getUserSongs: Song[];
}

export interface QueryWords {
  getWords: Words[];
  getWordsOfSong: Words[];
}
