import * as React from 'react';
import { AuthProvider } from './auth';
import { SongProvider } from './song';

const ContextProvider: React.FC<{}> = ({ children }) => {
  return (
    <SongProvider>
      <AuthProvider>{children}</AuthProvider>;
    </SongProvider>
  );
};

export default ContextProvider;
