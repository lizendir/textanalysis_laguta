import React from 'react';
import { Song } from '../models';

interface InitialStateProps {
  song: Song | null;
}

const initialState: InitialStateProps = {
  song: null,
};

type ContextType = {
  song: null | Song;
  setSong: (song: Song | null) => {};
};

export const SongContext = React.createContext<ContextType | undefined>(
  undefined,
);

function songReducer(state: { song: Song | null }, action: any) {
  switch (action.type) {
    case 'SET_SONG':
      return {
        ...state,
        song: action.payload,
      };
    default:
      return state;
  }
}

export function SongProvider(props: any) {
  const [state, dispatch] = React.useReducer(songReducer, initialState);

  function setSong(song: Song | null) {
    dispatch({
      type: 'SET_SONG',
      payload: song,
    });
  }

  return (
    <SongContext.Provider value={{ song: state.song, setSong }} {...props} />
  );
}
