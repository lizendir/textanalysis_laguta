import {
  Column,
  CreatedAt,
  Model,
  Table,
  UpdatedAt,
  DataType,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';
import { User } from './User';

@Table
export class Song extends Model<Song> {
  @Column
  name!: string;

  @Column
  wordCount!: number;

  @Column(DataType.TEXT)
  text!: string;

  @ForeignKey(() => User)
  @Column
  userId?: number;

  @BelongsTo(() => User, 'userId')
  user!: User;

  @CreatedAt
  createdAt!: Date;

  @UpdatedAt
  updatedAt!: Date;
}
