import { Request } from 'express';

import { Song } from './Song';
import { Words } from './Words';
import { SongWords } from './SongWords';
import { User } from './User';

export default [Song, Words, SongWords, User];

export interface ResolverModels {
  models: {
    Song: typeof Song;
    Words: typeof Words;
    SongWords: typeof SongWords;
  };
  req: Request;
}
