import { Model, Column, Table, ForeignKey } from 'sequelize-typescript';
import { Song } from './Song';
import { Words } from './Words';

@Table
export class SongWords extends Model<SongWords> {
  @ForeignKey(() => Song)
  @Column
  songId!: number;

  @ForeignKey(() => Words)
  @Column
  wordId!: number;
}
