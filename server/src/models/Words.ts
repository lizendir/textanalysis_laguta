import {
  Model,
  Column,
  Table,
  BelongsToMany,
  CreatedAt,
  UpdatedAt,
} from 'sequelize-typescript';

import { Song } from './Song';
import { SongWords } from './SongWords';

@Table
export class Words extends Model<Words> {
  @Column
  name!: string;

  @Column
  count!: number;

  @BelongsToMany(() => Song, () => SongWords)
  songs?: Song[];

  @CreatedAt
  @Column
  createdAt!: Date;

  @UpdatedAt
  @Column
  updatedAt!: Date;
}
