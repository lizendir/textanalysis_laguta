import {
  Model,
  Column,
  Table,
  CreatedAt,
  UpdatedAt,
  HasMany,
} from 'sequelize-typescript';
import { Song } from './Song';

@Table
export class User extends Model<User> {
  @Column
  username!: string;

  @Column
  password!: string;

  @Column
  email!: string;

  @HasMany(() => Song)
  songs?: Song[];

  @CreatedAt
  @Column
  createdAt!: Date;

  @UpdatedAt
  @Column
  updatedAt!: Date;
}
