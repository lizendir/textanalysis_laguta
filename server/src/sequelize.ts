import { Sequelize } from 'sequelize-typescript';
import { DB_NAME, DB_PASSWORD, DB_HOST, DB_USER } from './util/config';
import models from './models';

export const sequelize = new Sequelize({
  dialect: 'mysql',
  database: DB_NAME,
  username: DB_USER,
  password: DB_PASSWORD,
  host: DB_HOST,
  models
});
