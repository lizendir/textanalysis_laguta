import authSchema from './AuthenticationSchema';
import movieSchema from './TextAnalysisSchema';

export default [authSchema, movieSchema];
