import { gql } from 'apollo-server';

export default gql`
  scalar Date

  directive @auth on FIELD_DEFINITION

  type Song {
    id: Int!
    name: String!
    text: String!
    wordCount: Int!
    userId: Int!
  }

  input SongInput {
    name: String!
    text: String!
    userId: Int!
  }

  type Words {
    id: Int!
    songs: [Song!]
    name: String!
    count: Int!
  }

  type Query {
    getSongs: [Song!]
    getUserSongs(userId: Int!): [Song!] @auth
    getWords: [Words!]
    getWordsOfSong(songId: Int!): [Words!]
  }

  type Mutation {
    addSong(song: SongInput!): Song! @auth
  }
`;
