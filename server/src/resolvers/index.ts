import { song } from './song';
import { words } from './words';
import { scalarTypes } from './scalarTypes';
import { users } from './users';

export default [song, words, scalarTypes, users];
