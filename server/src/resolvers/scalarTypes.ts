import { IResolvers } from 'apollo-server';
import { GraphQLScalarType } from 'graphql';

import { ResolverModels } from '../models';

export const scalarTypes: IResolvers<{}, ResolverModels> = {
  Date: new GraphQLScalarType({
    name: 'Date',
    description: 'Date custom scalar type',
    parseValue(value: string) {
      return new Date(value); // value from the client
    },
    serialize(value: Date) {
      return value.toLocaleDateString(); // value sent to the client
    }
  })
};
