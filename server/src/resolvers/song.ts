import { IResolvers } from 'apollo-server';

import { ResolverModels } from '../models';
import { Song } from '../models/Song';
import { Words } from '../models/Words';

function createWordsRecords(splitedText: string[]) {
  const init: Words[] = [];

  return splitedText.reduce((acc, cur) => {
    const validWord = cur.replace(RegExp("[^\\w']", 'g'), '').toLowerCase();
    if (validWord == '') return [...acc];

    const hasWord = acc.some((word) => {
      return word.name == validWord;
    });

    if (hasWord) {
      const index = acc.findIndex((word) => {
        return word.name === validWord;
      });
      acc[index].count += 1;
      return [...acc];
    }

    return [
      {
        name: validWord,
        count: 1,
      } as Words,
      ...acc,
    ];
  }, init);
}

async function insertWordsRecords(words: Words[]) {
  return words.map((word) => {
    return Words.create(word);
  });
}

export const song: IResolvers<{}, ResolverModels> = {
  Query: {
    getSongs(_, {}, ctx) {
      return ctx.models.Song.findAll();
    },

    getUserSongs(_, args: { userId: number }, ctx) {
      return ctx.models.Song.findAll({ where: { userId: args.userId } });
    },
  },

  Mutation: {
    async addSong(_, args: { song: Song }, ctx) {
      const splitedText = args.song.text.split(' ');

      const words = createWordsRecords(splitedText);
      const createdWords = await insertWordsRecords(words);

      const song = await ctx.models.Song.create({
        ...args.song,
        wordCount: splitedText.length,
      });

      createdWords.forEach(async (word) => {
        (await word).$add('songs', song.id);
      });

      return song;
    },
  },
};
