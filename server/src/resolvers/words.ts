import { IResolvers } from 'apollo-server';

import { ResolverModels } from '../models';

export const words: IResolvers<{}, ResolverModels> = {
  Query: {
    async getWords(_, {}, ctx) {
      const words = await ctx.models.Words.findAll({
        include: [{ all: true }],
      });

      return words;
    },

    async getWordsOfSong(_, args: { songId: number }, ctx) {
      const wordsIds = await ctx.models.SongWords.findAll({
        where: { songId: args.songId },
      }).then((value) => {
        return value.map((songWords) => {
          return songWords.wordId;
        });
      });

      return ctx.models.Words.findAll({
        where: { id: wordsIds },
      });
    },
  },
};
