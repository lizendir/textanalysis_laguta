import bcrypt from 'bcryptjs';

import { User } from '../models/User';

export async function createUser(
  passwordToHash: string,
  email: string,
  username: string
) {
  const password = await bcrypt.hash(passwordToHash, 12);

  const newUser = new User({
    email,
    username,
    password,
    createdAt: new Date().toISOString()
  });

  return await newUser.save();
}
