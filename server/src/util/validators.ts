import { UserInputError } from 'apollo-server';
import bcrypt from 'bcryptjs';

import { User } from '../models/User';

export const validateRegisterInput = (
  username: string,
  email: string,
  password: string,
  confirmPassword: string
) => {
  let errors = {};
  if (username.trim() === '') {
    errors = {
      username: 'Username must not be empty'
    };
  }
  if (email.trim() === '') {
    errors = {
      ...errors,
      email: 'Email must not be empty'
    };
  } else {
    const regEx = /^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$/;
    if (!email.match(regEx)) {
      errors = {
        ...errors,
        email: 'Email must be a valid email address'
      };
    }
  }
  if (password === '') {
    errors = {
      ...errors,
      password: 'Password must not be empty'
    };
  } else if (password !== confirmPassword) {
    errors = {
      ...errors,
      confirmPassword: 'Passwords must match'
    };
  }

  if (!(Object.keys(errors).length < 1)) {
    throw new UserInputError('Errors', { errors });
  }
};

export const validateLoginInput = (username: string, password: string) => {
  let errors = {};
  if (username.trim() === '') {
    errors = {
      ...errors,
      username: 'Username must not be empty'
    };
  }
  if (password.trim() === '') {
    errors = {
      ...errors,
      password: 'Password must not be empty'
    };
  }

  if (!(Object.keys(errors).length < 1)) {
    throw new UserInputError('Errors', { errors });
  }
};

export async function validateCredentials(user: User, password: string) {
  const match = await bcrypt.compare(password, user.password);
  if (!match) {
    const errors = {
      general: 'Wrong crendetials'
    };
    throw new UserInputError('Wrong crendetials', { errors });
  }
}
