import * as dotenv from 'dotenv';

// To local development
let path = `${__dirname}/../../.env.development.local`;

dotenv.config();

export const DB_PASSWORD = process.env.REACT_APP_DB_PASSWORD;
export const DB_USER = process.env.REACT_APP_DB_USER;
export const SECRET_KEY = process.env.REACT_APP_SECRET_KEY || 'some_secret';
export const DB_NAME = process.env.REACT_APP_DB_NAME;
export const DB_HOST = process.env.REACT_APP_DB_HOST;
