import { User } from '../models/User';
import { prettyWomanText } from './PrettyWoman';
import { Words } from '../models/Words';
import { Song } from '../models/Song';

export const initDb = async () => {
  await User.create({
    password: '1111',
    email: 'user@mail.com',
    username: 'user',
  });

  const words = prettyWomanText.split(' ').reduce(
    (acc, word) => {
      const validWord = word.replace(RegExp("[^\\w']", 'g'), '').toLowerCase();
      if (validWord == '') return [...acc];
      return [validWord, ...acc];
    },
    ['a'],
  );

  const init: Words[] = [];
  const reducedWords = words.reduce((acc, cur) => {
    const hasWord = acc.some((word) => {
      return word.name == cur;
    });
    if (hasWord) {
      const index = acc.findIndex((word) => {
        return word.name === cur;
      });
      acc[index].count += 1;
      return [...acc];
    }
    return [
      ...acc,
      {
        count: 1,
        name: cur,
      } as Words,
    ];
  }, init);

  reducedWords.forEach((word) => {
    if (word.id) {
      Words.findByPk(word.id).then((value) => {
        if (value) {
          value = word;
          value.save();
        }
      });
    } else {
      Words.create(word);
    }
  });

  const song = await Song.create({
    name: 'Pretty Woman',
    text: prettyWomanText,
    userId: 1,
    wordCount: prettyWomanText.split(' ').length,
  } as Song);

  const wordsFromDb = await Words.findAll();

  wordsFromDb.forEach((word) => {
    word.$add('songs', song.id);
  });
};
