# README

This application can be running via docker or npm.

### Run via Docker

- Install docker and docker-compose on your system
- Run in terminal from this directory \$ docker-compose up
- You can use -d flag for detach mode

### Run via npm

- Intall Node.js on your system
- To start server you need to install MySql 8.0 on your system
- Before running an application need to install dependencies. For this run 'yarn' command in server and client diroctories.
- In server folder run 'npm run serve' or 'npm run start' to run server with nodemon
- In client folder run 'npm run start'
